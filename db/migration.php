<?php

define('ROOT', dirname(__DIR__));

require_once ROOT . '/system/main_console.php';

$sql = "
  CREATE TABLE IF NOT EXISTS banner_views (
    id SERIAL PRIMARY KEY,
    param1 VARCHAR(255),
    param2 VARCHAR(255),
    param3 VARCHAR(255),
    synchronized BOOLEAN DEFAULT FALSE
  ) ENGINE = innodb;
  
  CREATE INDEX banner_views_synchronized ON banner_statistics.banner_views(synchronized);
";

$pdo->exec($sql);
echo "Table `banner_views` was created.\n";
