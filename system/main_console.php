<?php

require_once ROOT . '/config/config.php';
require_once 'connection.php';
require_once 'utils.php';
require_once ROOT . '/services/banner_views_synchronizer.php';
require_once ROOT . '/vendor/rmccue/requests/library/Requests.php';

Requests::register_autoloader();
