<?php

class BannerViewsSynchronizer {
	const DEBUG = true; // В консоли будет выводиться отладочная информация
	protected $targetUrl = 'http://127.0.0.1:8001/back_system_service.php';
	protected $batchSize = 200; // Кол-во записей, которое за раз синхронизируем с бэк-системой
	protected $sendIntervalSeconds = 20; // Через сколько секунд повторно отправлять массив новых данных

	public function __construct($pdo)
	{
		$this->db = $pdo;
	}

	public function start()
	{
		echo "Banner views synchronizer has started.\n";
		while(true) {
			$this->sendBatchAndUpdateStatus();

			sleep($this->sendIntervalSeconds);
		}
	}

	protected function sendBatchAndUpdateStatus()
	{
		$savedIds = $this->sendBatch();

		if (!empty($savedIds)) {
			if (self::DEBUG) {
				$this->log('savedIds', $savedIds);
			}

			$this->updateRowsStatus($savedIds);
		}
	}

	/** Готовит данные и отправляет их
	 * @return array
	 */
	protected function sendBatch()
	{
		$dataForSend = $this->getDataForSend();
		if (self::DEBUG) {
			$this->log('dataForSend', $dataForSend);
		}

		return $this->post($dataForSend);
	}

	/** Обновляет значение поля synchronized на true у записей с указанными id
	 * @param array $savedIds - массив id записей
	 */
	protected function updateRowsStatus($savedIds)
	{
		$in  = str_repeat('?,', count($savedIds) - 1) . '?';
		$stmt = $this->db->prepare(
			"UPDATE banner_views SET synchronized = TRUE WHERE id IN ({$in})"
		);

		$stmt->execute($savedIds);
	}

	/** Возвращает массив с данными заданного размера из таблицы показа баннеров
	 * @return array
	 *   Пример:
	 *   [
	 *       55 => [
	 *         'param1' => 123,
	 *         'param2' => 222,
	 *         ...
	 *       ], ...
	 *   ]
	 */
	protected function getDataForSend()
	{
		$stmt = $this->db->prepare(
			'SELECT * FROM banner_views WHERE synchronized = FALSE LIMIT :limit'
		);

		$stmt->execute(['limit' => $this->batchSize]);

		$data = [];

		while ($row = $stmt->fetch(PDO::FETCH_LAZY))
		{
			$data[$row['id']] = [
				'param1' => $row['param1'],
				'param2' => $row['param2'],
				'param3' => $row['param3']
			];
		}
		return $data;
	}

	/** Делает POST-запрос с данными на указанный url
	 * @param array $data - ассоциативный массив с данными
	 * @return array - массив с Id сохраненных записей
	 */
	protected function post($data)
	{
		$response = Requests::post(
			$this->targetUrl, [], $data, ['timeout' => 20]
		);
		return (json_decode($response->body));
	}

	/** Выводит в консоль отладочную информацию
	 * @param $text
	 * @param $value
	 */
	protected function log($text, $value)
	{
		echo "== $text ==\n";
		print_r($value);
		echo "\n";
	}
}
