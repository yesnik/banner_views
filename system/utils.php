<?php
class Utils {
	/** Returns value for key in POST array
	 * @param $key
	 * @return null
	 */
	public static function getPostValue($key)
	{
		return isset($_POST[$key]) ? $_POST[$key] : null;
	}
}
