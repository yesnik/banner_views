<?php

require_once ROOT . '/config/config.php';

$dsn = 'mysql:host=' . $config['db']['host'] .
  ';dbname=' . $config['db']['database'] .
  ';charset=' . $config['db']['charset'];

$opt = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];
$pdo = new PDO($dsn, $config['db']['username'], $config['db']['password'], $opt);
