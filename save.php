<?php

if (empty($_POST)) {
    die();
}

require_once 'system/main.php';

$stmt = $pdo->prepare(
    "INSERT INTO banner_views(param1, param2, param3)
    VALUES(:param1, :param2, :param3)");

$stmt->execute(array(
    "param1" => Utils::getPostValue('param1'),
    "param2" => Utils::getPostValue('param2'),
    "param3" => Utils::getPostValue('param3')
));

$response = [
  'banner_view_id' => $pdo->lastInsertId()
];

header("Content-Type: application/json");
echo json_encode($response);
