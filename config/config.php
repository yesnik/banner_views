<?php

$config = [
  'db' => [
    'host' => '127.0.0.1',
    'username' => 'root',
    'password' => 'root',
    'database' => 'banner_statistics',
    'charset' => 'utf8'
  ]
];
