var banners = (function () {
    var saveUrl = 'http://127.0.0.1:8000/save.php',
        $responses,
        $btnStopRequests,
        sampleData = {param1: '1', param2: '2', param3: '3'},
        intervalId,
        counter = 0;

    function init() {
        $responses = $('#responses-holder');
        $btnStopRequests = $('#js-btn-stop-requests');

        bindEvents();
        start();
    }

    function bindEvents() {
        $btnStopRequests.on('click', disableInterval);
    }

    function start() {
        intervalId = setInterval(sendSampleData, 230);

        setTimeout(function () {
            disableInterval();
            alert("Минута закончилась. Отправка запросов остановлена. \n" +
                "Отправлено запросов: " + counter);
        }, 60000);
    }

    function sendSampleData() {
        $.post(saveUrl, sampleData);
        counter++;
    }

    function disableInterval() {
        clearTimeout(intervalId)
    }

    return {init: init};
})();

$(function () {
    banners.init()
});
